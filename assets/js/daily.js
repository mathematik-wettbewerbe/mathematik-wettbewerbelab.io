(function() {
	$('.daily div').each(function() {
		var d = new Date();
		$(this).children('img').attr('src', $(this).attr('path') + String(d.getMonth()+1).padStart(2, '0') + '-' + String(d.getDate()).padStart(2, '0') + '.png');
	});
})()
