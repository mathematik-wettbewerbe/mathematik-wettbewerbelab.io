---
title: "Mathematik Olympiade"
date: 2020-09-21T15:56:09+02:00
---
Landesrunde ab 5.Klasse in Flensburg am 22./23.Februar 2019, für die 3. und 4. Klasse an den Universitäten Flensburg und Lübeck (4. März)

Kontakt: Hinrich.Lorenzen@versanet.de

[Historie](Historie_MO.pdf)
