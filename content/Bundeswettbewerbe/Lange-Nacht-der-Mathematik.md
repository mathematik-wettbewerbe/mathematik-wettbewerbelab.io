---
title: "Lange Nacht der Mathematik"
date: 2020-09-21T16:24:08+02:00
---
Findet jeweils am Wochenende vor Totensonntag statt.

Weiter Informationen unter www.mathenacht.de.

## Geschichte
Seit August 1971 betrieb Herr Carow eine Mathematik-Arbeitsgemeinschaft. Lag die Teilnahmequote im ersten Jahrzehnt bei 20 und mehr, schrumpfte diese Zahl in den 1980ern trotz der Mitarbeit in der MA-THEMA.

Mitte der 90er Jahre haben Herr Zacharias (damals Klaus-Groth-Schule) und Herr Carow (damals Immanuel-Kant-Schule) ihre Gruppen zusammengelegt. Die Gruppenstärke lag bei ca. 10 und beinhaltete auch die Vorbereitung für Wettbewerbe insbesondere die Teilnahme an der Mathematik-Olympiade und am Känguru-Wettbewerb.

Die gemeinsame Gruppe zweier Gymnasien in Neumünster nahm am 14. Juni 1999 an einem Internet-Wettbewerb teil und empfand das als Herausforderung, in Schleswig-Holstein einen eigenen Wettbewerb zu organisieren, Aufgaben zu entwickeln und den Schülern der drei Neumünsteraner Schulen am 12./13.November 1999 anzubieten. Die Teilnehmerzahl stieg auf fast zweihundert am 1./2. Dezember 2000 mit weiteren Schulen u.a. aus Rendsburg und Kiel. Von da an wuchs die Teilnehmerzahl und die Anzahl der teilnehmenden Schulen. Schon bald nahmen neben den Partnerschulen aus Rumänien und Gruppen aus Helsinki auch Schulen aus dem übrigen Bundesgebiet teil. Herr Zacharias orientierte sich dann beruflich anders und somit übernahm Herr Carow die alleinige Organisation.

Mit der Pensionierung ist es vor allem eine große Herausforderung für Herrn Carow, genügend „Personal“ für die Ausrichtung dieses Wettbewerbs zu finden.

Da wir nicht alle Aufgaben von über 4000 Gruppen korrigieren können, gibt es Vorrunden, die elektronisch ausgewertet werden. Diese Programmierung haben in den letzten Jahren Robert Fink, René Woltmann, Marc Philip von See, Thies Töbermann, Dennis Schütt, Oliver Sievers übernommen. Seit 2011 helfen Meinert Boy Leinigen und Simon Kurka aus Oldenburg (in Oldenburg) tatkräftig mit, alle technischen Probleme zu lösen. Simon hat die komplette Seite von Grund auf neu programmiert und betreut die Datenbank. Meinert hat die Inhalte aktualisiert und betreut das Forum.

![Statistik Teilnehmerzahlen](teilnehmerzahlen.png)

[Historie](Historie_LNdM.pdf)
