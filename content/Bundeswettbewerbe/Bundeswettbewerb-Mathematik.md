---
title: "Bundeswettbewerb Mathematik"
date: 2020-09-21T15:55:27+02:00
---
## Für wen ist der Wettbewerb gedacht?
Der Bundeswettbewerb Mathematik ist ein mathematischer Schülerwettbewerb für alle an Mathematik Interessierten. Er besteht aus zwei Hausaufgabenrunden und einer abschließenden dritten Runde, die aus einem mathematischen Fachgespräch besteht. Der Wettbewerb richtet sich an Schülerinnen und Schüler, die eine zur allgemeinen Hochschulreife führende Schule besuchen. Mit interessanten und anspruchsvollen Aufgaben möchte er sie anregen, sich eine Zeit lang intensiv mit Mathematik zu beschäftigen. Neben dem mathematischen Schulwissen muss man zur Teilnahme vor allem auch etwas Ausdauer mitbringen.

## Wie läuft der Wettbewerb ab?
In den beiden Hausaufgabenrunden werden jeweils vier Aufgaben aus unterschiedlichen Bereichen der Elementarmathematik (Geometrie, Kombinatorik, Zahlentheorie, Algebra) gestellt, die in der zweiten Runde deutlich anspruchsvoller sind als in der ersten. Sie müssen jeweils in ca. zwei Monaten in Hausarbeit selbstständig gelöst und schriftlich ausgearbeitet werden.

In der ersten Runde sind auch Gruppenarbeiten zugelassen, die allerdings das Korrekturverfahren außer Konkurrenz durchlaufen und deshalb auch nicht mit Preisen versehen werden.

In der dritten Runde, auch Kolloquium genannt, geht es nicht mehr um das Lösen von Aufgaben. Hier führt jeder Teilnehmer und jede Teilnehmerin ein knapp einständiges Fachgespräch mit je einem Mathematiker bzw. einer Mathematikerin aus Universität und Schule. Auf der Basis dieser Gespräche werden die Bundessieger/innen ausgewählt. Daneben gestalten die Teilnehmerinnen und Teilnehmer zum gegenseitigen Kennenlernen ein Rahmenprogramm mit ganz unterschiedlichen Beiträgen. Außerdem haben die Teilnehmer/innen ein integriertes Auswahlverfahren zur Aufnahme in die Studienstiftung des deutschen Volkes durchlaufen.

## Und so läuft der Wettbewerb zeitlich ab:
|Dezember		|Ausschreibung, Versand der Unterlagen				|		|
|---			|---								|---		|
|bis Ende Februar	|Bearbeitung der Aufgaben durch die Teilnehmer/innen		|		|
|1. März		|Einsendeschluss						|1. Runde	|
|Anfang Juni		|Mitteilung der Korrekturergebnisse an die Teilnehmer/innen	|		|
|Anfang Juni		|Aufgabenstellung, Versand direkt an die Teilnahmeberechtigten	|		|
|bis Ende August	|Bearbeitung der Aufgaben durch die Teilnehmer/innen		|		|
|1. September		|Einsendeschluss						|2. Runde	|
|Anfang November	|Mitteilung der Korrekturergebnisse an die Teilnehmer/innen	|		|
|Anfang Februar		|Kolloquium							|3. Runde	|

## Wer kann an den einzelnen Runden teilnehmen?
Die erste Runde steht Schüler/innen aller Klassenstufen an den oben genannten Schulen offen. In seinen inhaltlichen Anforderungen orientiert sich der Wettbewerb jedoch an den Klassen 10 bis 13.
Alle Preisträger/innen der ersten Runde sind berechtigt, an der zweiten Runde teilzunehmen.
Die ersten Preisträger/innen der zweiten Runde haben sich für die Teilnahme an der dritten Runde qualifiziert.

## Was gibt es zu gewinnen?
In der ersten Runde gibt es Urkunden für erste, zweite und dritte Preise.
In der zweiten Runde gibt es die gleichen Preisstufen wie in der ersten aber zusätzlich zu den Urkunden Geldpreise bis zu 160 EURO.

In der dritten Runde gibt es nur eine Preisstufe, die Preisträger/innen werden Bundessieger/innen genannt. Sie werden mit Beginn eines Studiums in die Förderung der Studienstiftung des deutschen Volkes (www.studienstiftung.de) aufgenommen. Sie erhalten damit ein Stipendium und vielfältige Unterstützung im Studium.
Darüber hinaus winken verschiedene Sonderpreise.

Weitere Informationen unter http://www.bundeswettbewerb-mathematik.de
oder Kontaktaufnahme info@bundeswettbewerb-mathematik.de
